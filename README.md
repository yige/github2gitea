# github2gitea

Github 仓库迁移至 Gitea 脚本

迁移 github 私有/公有仓库到 gitea 是一件容易的事情，不过由于网络原因，部分较大的项目可能会失败。此脚本会自动过滤已经迁移完成的仓库，因此重复执行脚本直至 gitea 中的仓库和 github 中的仓库数量一致，即表示迁移完成。gitea 迁移失败的仓库会被系统自动删除，因此可能出现仓库数量浮动。为了确保代码资产安全，您在迁移完成后应该间隔十分钟再次执行，直到 gitea 中的仓库和 github 中的仓库数量仍然一致。

需要注意，使用该脚本前，需创建一个Github Token，并且需要临时关闭Two-factor authentication。



Python3:
     
```python
import requests
from requests.adapters import HTTPAdapter
import time
import json

GITEA_URL = 'https://domain'
GITEA_USERNAME = 'username'
GITEA_PASSWORD = '***'
GITEA_ORGNAME_OR_USERNAME = 'orgName'
GITHUB_ORGNAME = 'orgName'
GITHUB_TOKEN = '***************************************'
GITHUB_USERNAME = 'yi-ge'
GITHUB_PASSWORD = '***'

finishedRepo = json.load(open('finishedRepo.json', 'r'))

# Get user Gitea
response_user = requests.get(
    f'{GITEA_URL}/api/v1/users/{GITEA_ORGNAME_OR_USERNAME}')

verify = False

if response_user.status_code == 200:
    uid = response_user.json()['id']
    for n in range(1000):
        page = n + 1
        n = page
        response_github = requests.get(
            f'https://api.github.com/orgs/{GITHUB_ORGNAME}/repos?' +
            f'per_page=100&page={page}',
            headers={'Authorization': 'token ' + GITHUB_TOKEN})

        # List Github repos
        if response_github.status_code == 200:
            if len(response_github.json()):
                for repo in response_github.json():
                    repo_clone_url = repo['clone_url']
                    repo_name = repo['name']
                    description = repo['description']

                    print('Creating repository: ' + repo_name)
                    print(repo_clone_url)

                    s = requests.Session()
                    s.mount('http://', HTTPAdapter(max_retries=3))
                    s.mount('https://', HTTPAdapter(max_retries=3))

                    response_migrate = s.post(
                        f'{GITEA_URL}/api/v1/repos/migrate',
                        json={
                            'clone_addr': repo_clone_url.replace(
                                'https://github.com',
                                f'https://{GITHUB_USERNAME}:' +
                                GITEA_PASSWORD +
                                '@github.com'),
                            'mirror': False,
                            'private': True,
                            "issues": True,
                            "labels": True,
                            "milestones": True,
                            "pull_requests": True,
                            "releases": True,
                            "wiki": True,
                            'repo_name': repo_name,
                            'uid': uid,
                            'description': description
                        },
                        auth=(GITEA_USERNAME, GITEA_PASSWORD),
                        timeout=120
                    )
                    if (response_migrate.status_code == 409
                            or response_migrate.json()['id'] > 0):
                        finishedRepo.append({
                            'name': repo_clone_url,
                            'time': time.strftime(
                                '%Y-%m-%d %H:%M:%S',
                                time.localtime()
                            ),
                            'created':
                                response_migrate.status_code == 409
                        })
                        open('finishedRepo.json', 'w').write(
                            json.dumps(finishedRepo))
                        if (response_migrate.status_code == 409):
                            print(
                                repo_name +
                                ': The repository already exists.'
                            )
                        else:
                            print(
                                repo_name +
                                ': Clone repository created!'
                            )
                            # time.sleep(8)
                    else:
                        print(repo_name + ': Clone repository error!')
                        print(response_migrate.json())
            else:
                if verify:
                    print('All clone finished!')
                else:
                    page = 1
                    verify = True
        else:
            print(response_github.status_code)
            print('Error list Github repos.')
else:
    print('Error user Gitea.')
```
