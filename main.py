import requests
from requests.adapters import HTTPAdapter
import time
import json

GITEA_URL = 'https://domain'
GITEA_USERNAME = 'username'
GITEA_PASSWORD = '***'
GITEA_ORGNAME_OR_USERNAME = 'orgName'
GITHUB_ORGNAME = 'orgName'
GITHUB_TOKEN = '***************************************'
GITHUB_USERNAME = 'yi-ge'
GITHUB_PASSWORD = '***'

finishedRepo = json.load(open('finishedRepo.json', 'r'))

# Get user Gitea
response_user = requests.get(
    f'{GITEA_URL}/api/v1/users/{GITEA_ORGNAME_OR_USERNAME}')

verify = False

if response_user.status_code == 200:
    uid = response_user.json()['id']
    for n in range(1000):
        page = n + 1
        n = page
        response_github = requests.get(
            f'https://api.github.com/orgs/{GITHUB_ORGNAME}/repos?' +
            f'per_page=100&page={page}',
            headers={'Authorization': 'token ' + GITHUB_TOKEN})

        # List Github repos
        if response_github.status_code == 200:
            if len(response_github.json()):
                for repo in response_github.json():
                    repo_clone_url = repo['clone_url']
                    repo_name = repo['name']
                    description = repo['description']

                    print('Creating repository: ' + repo_name)
                    print(repo_clone_url)

                    s = requests.Session()
                    s.mount('http://', HTTPAdapter(max_retries=3))
                    s.mount('https://', HTTPAdapter(max_retries=3))

                    response_migrate = s.post(
                        f'{GITEA_URL}/api/v1/repos/migrate',
                        json={
                            'clone_addr': repo_clone_url.replace(
                                'https://github.com',
                                f'https://{GITHUB_USERNAME}:' +
                                GITEA_PASSWORD +
                                '@github.com'),
                            'mirror': False,
                            'private': True,
                            "issues": True,
                            "labels": True,
                            "milestones": True,
                            "pull_requests": True,
                            "releases": True,
                            "wiki": True,
                            'repo_name': repo_name,
                            'uid': uid,
                            'description': description
                        },
                        auth=(GITEA_USERNAME, GITEA_PASSWORD),
                        timeout=120
                    )
                    if (response_migrate.status_code == 409
                            or response_migrate.json()['id'] > 0):
                        finishedRepo.append({
                            'name': repo_clone_url,
                            'time': time.strftime(
                                '%Y-%m-%d %H:%M:%S',
                                time.localtime()
                            ),
                            'created':
                                response_migrate.status_code == 409
                        })
                        open('finishedRepo.json', 'w').write(
                            json.dumps(finishedRepo))
                        if (response_migrate.status_code == 409):
                            print(
                                repo_name +
                                ': The repository already exists.'
                            )
                        else:
                            print(
                                repo_name +
                                ': Clone repository created!'
                            )
                            # time.sleep(8)
                    else:
                        print(repo_name + ': Clone repository error!')
                        print(response_migrate.json())
            else:
                if verify:
                    print('All clone finished!')
                else:
                    page = 1
                    verify = True
        else:
            print(response_github.status_code)
            print('Error list Github repos.')
else:
    print('Error user Gitea.')
